# hertz 2
## Question
>This flag has been encrypted with some kind of cipher, can you decrypt it? Connect with nc 2018shell.picoctf.com 39961.
### Hint
>These kinds of problems are solved with a frequency that merits some analysis.

## Solution
ciphertext
~~~
Let's decode this now!
Orx wevuc knfla bfg medph fixn orx sjzq tfy. V uja'o kxsvxix orvh vh heur ja xjhq pnfksxd va Pvuf. Vo'h jsdfho jh vb V hfsixt j pnfksxd jsnxjtq! Fcjq, bvax. Rxnx'h orx bsjy: pvufUOB{hekhovoeovfa_uvprxnh_jnx_off_xjhq_tayjfldiqx}
~~~
Here is a useful tool to solve this problem.
~~~
https://quipqiup.com
~~~
Clues
~~~
pvufUOB=picoCTF
~~~
```
picoCTF{substitution_ciphers_are_too_easy_gndaowmvye}
```
