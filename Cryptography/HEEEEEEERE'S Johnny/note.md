# HEEEEEEERE'S Johnny!
## Question
> Okay, so we found some important looking files on a linux computer. Maybe they can be used to get a password to the process. Connect with ```nc 2018shell.picoctf.com 40157```. Files can be found here: [passwd](./resource/passwd) [shadow](./resource/shadow). 

## Hint
> If at first you don't succeed, try, try again. And again. And again. <br>
> If you're not careful these kind of problems can really "rockyou".
## Solution
#### 1.
In Linux */etc/passwd* is a text file that contains the *attributes* of each user or account. <br>
The permissions for /etc/passwd are by default set so that it is *world readable*.
```console
root@kali:~# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin

```
fist line means :the user's *login name* is **root**,password **x** means password is *shadowed* & *visible* in */etc/shadow*  (the password has been assigned to the user and is required for authentication.). <br> <br>
#### 2.
Then we try to know what's in */etc/shadow*. <br>
And we found it passwd is *hashed*. <br>
So throught hint *(try ,try again)*, we can know that this is a topic using **brute-force**,we need to crack the hashed password.
```console
root@kali:~# cat /etc/shadow
root:$6$z8lrl2345yvV1Y$mee82emcWuVJabcdexrZRoW9BdKWmOjck/CObiM.Cwa24zL7BIQQAKFittPwyhahahaUkNnoE4Yh/:18169:0:99999:7::
```
### Tool
[John the Ripper](https://tools.kali.org/password-attacks/john) (JtR) <br>
> JtR is a great way to show if you (or your users) have weak/predictable passwords!

##### #unshadow
How to use
```console
root@kali:~/Documents/heres_johnny# unshadow
Usage: unshadow PASSWORD-FILE SHADOW-FILE
```
Save to [*pico_user.txt*](./resource/pico_user.txt)
```console
root@kali:~/Documents/heres_johnny# ls
JohnTheRipper  passwd  shadow
root@kali:~/Documents/heres_johnny# unshadow passwd shadow
root:$6$IGI9prWh$ZHToiAnzeD1Swp.zQzJ/Gv.iViy39EmjVsg3nsZlfejvrAjhmp5jY.1N6aRbjFJVQX8hHmTh7Oly3NzogaH8c1:0:0:root:/root:/bin/bash
root@kali:~/Documents/heres_johnny# unshadow passwd shadow > pico_user.txt
root@kali:~/Documents/heres_johnny# ls
JohnTheRipper  passwd  pico_user.txt  shadow
```
##### #john
Use **john** to carck the pico_user.txt ,then we get the password : ***password1***
```console
root@kali:~/Documents/heres_johnny# john pico_user.txt 
Warning: detected hash type "sha512crypt", but the string is also recognized as "HMAC-SHA256"
Use the "--format=HMAC-SHA256" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 2 OpenMP threads
Proceeding with single, rules:Single
Press 'q' or Ctrl-C to abort, almost any other key for status
Warning: Only 2 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 6 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 4 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 5 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 3 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 5 candidates buffered for the current salt, minimum 8 needed for performance.
Warning: Only 7 candidates buffered for the current salt, minimum 8 needed for performance.
Almost done: Processing the remaining buffered candidate passwords, if any.
Proceeding with wordlist:/usr/share/john/password.lst, rules:Wordlist
password1        (root)
1g 0:00:00:00 DONE 2/3 (2019-09-30 15:49) 1.190g/s 1295p/s 1295c/s 1295C/s 123456..franklin
Use the "--show" option to display all of the cracked passwords reliably
Session completed

```
### Finally
We get the root's password ,then try to login the server to get the flag~
```console
root@kali:~# nc 2018shell.picoctf.com 40157
Username: root
Password: password1
picoCTF{J0hn_1$_R1pp3d_1b25af80}
```
## flag
```
picoCTF{J0hn_1$_R1pp3d_1b25af80}
```

