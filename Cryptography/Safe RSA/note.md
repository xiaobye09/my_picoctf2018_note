# caesar cipher 2 
## Question
>Now that you know about RSA can you help us decrypt this [ciphertext](./ciphertext)? We don't have the decryption key but something about those values looks funky..


### Hint
>RSA [tutorial](https://en.wikipedia.org/wiki/RSA_(cryptosystem))  
>Hmmm that e value looks kinda small right?  
>These are some really big numbers.. Make sure you're using functions that don't lose any precision!  
## Solution
hint: Using gmpy2(which is based on GNU Multiple Precision Arithmetic Library) to perform n-th root operation can check if result is  exact(precise)


[sol.py](./sol.py)

## flag
```
picoCTF{e_w4y_t00_sm411_7815e4a7}
```
