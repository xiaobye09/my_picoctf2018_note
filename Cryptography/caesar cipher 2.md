# caesar cipher 2 
## Question
>Can you help us decrypt this [message](./caesar%20cipher%202/ciphertext)? We believe it is a form of a caesar cipher. You can find the ciphertext in /problems/caesar-cipher-2_3_4a1aa2a4d0f79a1f8e9a29319250740a on the shell server.
### Hint
>You'll have figure out the correct alphabet that was used to encrypt the ciphertext from the ascii character set  
>[ASCII Table](https://www.asciitable.com/)  
## Solution
First,I calculate the offset of the encryption.   
```python
a = "4-'3evh?"  #picoCTF{
b = "picoCTF{"
for i in range(len(b)):
    print "%c:%d , %c sub %c:%d" %(a[i],ord(a[i]),b[i],a[i],ord(b[i])-ord(a[i]))
```
And I got the result below.   
We can see that their are two different offsets.  
I presume that if the character in cipher text larger than a specific number,we have to add -34 to our it,otherwise,we add 60.  
```bash
4:52 , p sub 4:60
-:45 , i sub -:60
':39 , c sub ':60
3:51 , o sub 3:60
e:101 , C sub e:-34
v:118 , T sub v:-34
h:104 , F sub h:-34
?:63 , { sub ?:60
```
Then I set the specific number to 64.   
```python
cipher="4-'3evh?'c)7%t#e-r,g6u#.9uv#%tg2v#7g'w6gA"

plain = ""
for c in cipher:
    if ord(c)>64:
        plain+=chr( ord(c)-34 )
    else:
        plain+=chr( ord(c)+60)

print plain
```
```
picoCTF{cAesaR_CiPhErS_juST_aREnT_sEcUrE
```
We miss a character '}' and the corresponding cipher character is 'A' (65),so we replace 64 with 65.
```python 
cipher="4-'3evh?'c)7%t#e-r,g6u#.9uv#%tg2v#7g'w6gA"

plain = ""
for c in cipher:
    if ord(c)>65:
        plain+=chr( ord(c)-34 )
    else:
        plain+=chr( ord(c)+60)

print plain
```
Finally,I got the full flag.

## Flag
```
picoCTF{cAesaR_CiPhErS_juST_aREnT_sEcUrE}
```