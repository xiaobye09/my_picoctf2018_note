basic linux command:https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners
#### Tool
- # readelf
>To get infomation of elf file

- # checksec
>To check the security mechanism of the elf
- # xxd 
>creates a hex dump of a given file or standard input.  It can also convert a hex dump back  
>to its original binary form.  Like uuencode(1) and uudecode(1) it allows  the  transmission  of  
>binary  data  in a `mail-safe' ASCII representation, but has the advantage of decoding to standard   
>output.  Moreover, it can be used to perform binary file patching.  
- # sysctl
>sysctl  is  used  to  modify  kernel parameters at runtime.  The parameters available are those  
>listed under /proc/sys/.  Procfs is required for sysctl support in Linux.  You can  use  sysctl  
>to both read and write sysctl data.
- # uname
>print system information
- # echo $SHELL
>find out what shell is being used
- # pwd(print working directory)
>When you first open the terminal, you are in the home directory of your user. To know which directory you are in, you can use the “pwd” command. It gives us the absolute path, which means the path that starts from the root. The root is the base of the Linux file system. It is denoted by a forward slash( / ). The user directory is usually something like "/home/username".   

p.s.$PWD is the Pathname of the current Working Directory
- # cd dest
    1. ### cd :To home directory
    2. ### cd / :To root
    3. ### cd ../ :To previous directory
    4. ### cd ~/  :To home
- # To get user name on linux
    1. ### id -u -n  (-u can get user id ,and -n can show show user name by user id)
    2. ### echo "$USER"
    3. ### w (display current user info.)
    4. ### who(display current users info.)
    5. ### whoami(display current user name)
<br>

- # binwalk :  
> tool for searching binary images for embedded files and executable code.(apt-get install binwalk)

### Some useful parameter  
1. #### -e :
> Automatically extract known file types.
2. #### -D=&lt;type:ext:cmd&gt; :
> extract <type> signatures ,give the files on extension of        &lt;ext&gt; and execute &lt;cmd&gt;
    
e.g 

```
binwalk -D='jpeg:jpeg' file
```

### p.s. we can achieve the same goal by dd,too.(hackfun_org_2017_01_12_CTF_E4_B8_AD_E5_9B_BE_E7_89_87_E9_9A_.pdf)
<br>

- ## printenv : 
> Print the values of specified enviroment VARIABLE(s).If no VARIABLE is specified ,print name and value pairs for them all.

### p.s. we can achieve the same goal by command 'env'.

<br>

- # xclip : 
> Reads from standard in,or from one or more files ,and makes the data available as an X selection for pasting into X applications.Prints current X selection to standard out.
    e.g.(use three fingers to press the touchpad to paste it)
    

```
ls|xclip
```

e.g.(copy the text to clipboard )


```
ls|xclip
```

### p.s : operator | is pipe operator and is a form of redirection (transfer of standard output to some other destination) that is used in Linux and other Unix-like operating systems to send the output of one command/program/process to another 

<br>

- # hexedit :
> hexadecimal editor

- # exiftool :
> Read and write meta information in files

- # retext : 
> markdown editor

- ## touch :  
> Update the access and modification times of each FILE to the current time.A FILE argument that does not exist is created empty, unless -c or -h is supplied.A  FILE  argument  string of - is handled specially and causes touch to change the times of the file associate with standard output.Mandatory arguments to long options are mandatory for short options too.

- # locate : 
> The locate command is used to locate a file on linux,just like search command on windows(If you can't find the target file,execute sudo updatedb and try again)
### some useful parameters
1. #### -i : 
> ignore the case (it doesn't matter if it is uppercase or lowercase)


e.g.To find file hacking_DIY.py

```
locate hack*hack*
```
#### p.s.use asterisk(*) can sperate the key word of file name      
<br>

- # cat : 
> To display the contents of a file.It is usually used to easily view programs.

- # unshadow : 
> The  unshadow  tool combines the passwd and shadow files so <span style="background-color: #FFFF00">John</span> can use them. You might need this since if you only used your shadow file, the GECOS information wouldn't be used by the "single crack"  mode,  and  also  you wouldn't be able to use the '-shells' option. On a normal system you'll need to run unshadow as root to be able to read the shadow file.  


```
unshadow passwd shadow
```             

### p.s:shadow is in /etc/shadow  and  password is in /etc/passwd
     
<br>

- # nc(netcat) : 
> a kind of TCP/UDP application is used to scan port ,FTP ,chat ,remote control ,etc.
    e.g.self chat room
    
```
server:
    nc -l localhost 888
```

```
client:
    nc localhost 8888
```


e.g.port scan

```
    nc -z -v -n ip_address port1-port2
```

- #### -z:scan without sending data
- #### -v:display scanning info.
- #### -n:not doing DNS search

<br>

- # grep: 
>search for PATTERN in each FILE or standard input.
### some useful parameter
1. #### -F:
> Interpret  PATTERN  as  a  list  of fixed strings (instead of regular expressions),separated by newlines, any of which is to be matched.

```    
grep -F 'string1    
string2,  
string3,  
...,  
stringn' FILE  
```
        
2. #### -e PATTERN:
> Use PATTERN as the pattern.  If this option is used multiple times or  is  combined with  the  -f  (--file)  option, search for all patterns given.  This option can be used to protect a pattern beginning with “-”

3. #### -i:
> Ignore case distinctions, so that characters that differ only in  case  match  each other.

4. #### -v:
> Invert the sense of matching, to select non-matching lines.

5. #### -A NUM:
> Print NUM lines of trailing context after matching lines.  Places a line containing a  group  separator  (--)  between  contiguous  groups  of matches.  With the -o or --only-matching option, this has no effect and a warning is given.

6. #### -B NUM:
> Print NUM lines of leading context before matching lines.  Places a line containing a  group  separator  (--)  between  contiguous  groups  of matches.  With the -o or--only-matching option, this has no effect and a warning is given.
7. #### -a, --text
>Process a binary file as if it were text; this is equivalent to the  --binary-files=text option.

### Regular expression
1. #### [^]:
> the characters after ^ is not one of those included within the square brackets.
e.g.  
```
grep [abc] FILE  <=>  grep [^d-z] FILE
```
2. #### []:a set of characters.Only represent one charactorer in the regular expression.
e.g.  
```
grep [a-bA-B0-9] FILE
```
```
grep [a-b] FILE
```
```
grep [0-9] FILE
```
```
grep [A-B] FILE
```
3. #### .(dot)
> a single character,it could be any character.
4. #### *(asterisk)
> the preceding character matches 0 or more times.
5. #### {}
> {n} - the preceding character matches exactly n times.  
> {n,m} - the preceding character matches at least n times and not more than m times.
e.g.
```
grep -n 'a\{3,6\}' FILE
```
6. #### ^:
> matches the beginning of the line.(This symbol represent the begining of the line)

7. #### $:
> matches the end of the line.(This symbol represent the end of the line)

### The regexp below are extended regular expression,if we use them,we have to add -E before.
1. #### +:
> the preceding character matches 1 or more times.
2. #### ?:
> the preceding character matches 0 or 1 times only.
3. #### | (pipe symbol) 
> the logical OR operation.
4. #### ():
> allows us to group several characters to behave as one.
5. #### ()+:
> the characters group matches 1 or more times.
e.g. 
```
grep -E '(aba)+' FILE
```
#### p.s. since a-z <=> [:lower:] , [a-z] <=> [[:lowerL]]

- # objdump
>display information from object files.
### useful parameter
1. #### -M intel
>display in intel syntex
2. #### -d
>Display the assembler mnemonics for the machine instructions from objfile.  This option only disassembles those sections which are expected to contain instructions.
3. #### -D 
>Like -d, but disassemble the contents of all sections, not just those expected to contain instructions.


