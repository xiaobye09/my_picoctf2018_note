# what base is this?
## Question
>To be successful on your mission, you must be able read data represented in different ways, such as hexadecimal or binary. Can you get the flag from this program to prove you are ready? Connect with nc 2018shell.picoctf.com 64706.
### Hint
>I hear python is a good means (among many) to convert things.   
>It might help to have multiple windows open   
## Solution
### Solution 1:
```python
import binascii
def f():
    x=raw_input()               #treat input as string
    x=x.replace(' ','')         #remove space in string
    x=int(x,2)                  #convert binary string to decimal
    x='%x' %x                   #convert x to hexidecimal string, '%x' %x means read x as hexcidecimal string
    print binascii.unhexlify(x) #convert hexidecimal string to binary data

def f1():
    x=raw_input()               #treat input as string
    print binascii.unhexlify(x)   


def f2():
    x=raw_input()               #treat input as string
    x=x.split(" ")              #split string by " " 
    for i in x:
        print binascii.unhexlify('%x' %int(i,8))


f()
f1()
f2()
```
# solution 2:
```python
from pwn import *
# Don't need to import os,sys,time,requests,re,random since pwn will import them automatically

s = remote("2018shell.picoctf.com",64706)

#binary
m = s.recvuntil('as a word.')
print 'TEST '+ m

t = re.findall(r'[0-1]+',m)
ans = ''
for b in t:
    ans+=chr(int(b,2))

print "SEND "+ans
s.sendline(ans)

#hexidecimal
m=s.recvuntil('as a word.')
print 'TEST '+m

t = re.findall(r'([0-9a-f]+) as ',m)[0].split(" ")[0]
ans=t.decode('hex')

print "SEND "+ans
s.sendline(ans)

#octal
m=s.recvuntil('as a word.')
print 'TEST '+m

ans=""
t=re.findall(' [0-7]+',m)
for i in t:
    ans+=chr(int(i,8))

print "SEND "+ ans
s.sendline(ans)

flag=s.recvall()
print 'TEST '+ flag
```
## Flag
```
picoCTF{delusions_about_finding_values_5b21aa05}
```
<br><br>

- ## Useful tips   
    
    - ### python     
        - #### raw string
        >Python raw string is created by prefixing a string literal with ‘r’ or ‘R’. Python raw string treats backslash \ as a literal character. This is useful when we want to have a string that contains backslash and don’t want it to be treated as an escape character.
    - ### learn some regular expression is a good idea!

- ## Useful function
    - ### pwntools
        - #### recv(numb = 4096, timeout = default) → str
        >Receives up to numb bytes of data from the tube, and returns as soon as any quantity of data is available.
        - #### recvall() → str
        >Receives data until EOF is reached.
        - #### recvline(keepends = True) → str
        >Receive a single line from the tube.
        - #### recvuntil(delims, timeout = default) → str
        >Receive data until one of delims is encountered.
        - #### remote(host, port, fam='any', typ='tcp', ssl=False, sock=None, *args, **kwargs)
        >Bases: pwnlib.tubes.sock.sock    
        Creates a TCP or UDP-connection to a remote host. It supports both IPv4 and IPv6.   
        The returned object supports all the methods from pwnlib.tubes.sock and pwnlib.tubes.tube.   
        - #### close()
        >Closes the tube
    - ### re
        - #### re.findall(pattern, string, flags=0)
        >Return all non-overlapping matches of pattern in string, as a list of strings. The string is scanned left-to-right, and matches are returned in the order found. If one or more groups are present in the pattern, return a list of groups; this will be a list of tuples if the pattern has more than one group. Empty matches are included in the result.
        - #### re.search(pattern, string, flags=0)
        >Scan through string looking for the first location where the regular expression pattern produces a match, and return a corresponding match object. Return None if no position in the string matches the pattern; note that this is different from finding a zero-length match at some point in the string.
        - #### Match.group([group1, ...])
        >Returns one or more subgroups of the match. If there is a single argument, the result is a single string; if there are multiple arguments, the result is a tuple with one item per argument. Without arguments, group1 defaults to zero (the whole match is returned). <u>**If a groupN argument is zero, the corresponding return value is the entire matching string**</u>; if it is in the inclusive range [1..99], it is the string matching the <u>**corresponding parenthesized group**</u>. If a group number is negative or larger than the number of groups defined in the pattern, an IndexError exception is raised. If a group is contained in a part of the pattern that did not match, the corresponding result is None. If a group is contained in a part of the pattern that matched multiple times, the last match is returned.
        ```python
        >>> m = re.match(r"(\w+) (\w+)", "Isaac Newton, physicist")   #The first (\w+) represent to subgroup 1
        >>> m.group(0)       # The entire match
        'Isaac Newton'
        >>> m.group(1)       # The first parenthesized subgroup.
        'Isaac'
        >>> m.group(2)       # The second parenthesized subgroup.
        'Newton'
        >>> m.group(1, 2)    # Multiple arguments give us a tuple.
        ('Isaac', 'Newton')
        

<br><br>
- ## Something that I encounter during solving the problem
My code:
```python
from pwn import *
# Don't need to import os,sys,time,requests,re,random since pwn will import them automatically

s = remote("2018shell.picoctf.com",64706)

#binary
m = s.recvuntil('as a word.')
print 'TEST '+ m

t = re.findall(r'[0-1]+',m)
ans = ''
for b in t:
    ans+=chr(int(b,2))

print "SEND "+ans
s.sendline(ans)

#hexidecimal
m=s.recvuntil('as a word.')
print 'TEST '+m

t = re.findall(r'([0-9a-f]+) as ',m)[0].split(" ")[0]
ans=t.decode('hex')

print "SEND "+ans
s.sendline(ans)

#octal
m=s.recvuntil('as a word.')
print 'TEST '+m

ans=""
t=re.findall(' [0-7]+',m)
for i in t:
    ans+=chr(int(i,8))

print "SEND "+ ans
s.sendline(ans)

flag=s.recv()
print 'TEST '+ flag
```
Result:
```bash
[x] Opening connection to 2018shell.picoctf.com on port 64706
[x] Opening connection to 2018shell.picoctf.com on port 64706: Trying 18.188.70.152
[+] Opening connection to 2018shell.picoctf.com on port 64706: Done
TEST We are going to start at the very beginning and make sure you understand how data is stored.
wichita
Please give me the 01110111 01101001 01100011 01101000 01101001 01110100 01100001 as a word.
SEND wichita
TEST 
To make things interesting, you have 30 seconds.
Input:
Please give me the 747572746c65 as a word.
SEND turtle
TEST 
Input:
Please give me the  142 157 164 164 154 145 as a word.
SEND bottle
TEST 
Input:

[*] Closed connection to 2018shell.picoctf.com port 64706

```
Where is my flag!!??I thought it should be at the end of line after execute "flag=s.recv()".But it didn't. So,what happen to my program?  
After lots of research on this problem,I found that the last line "Input:" is the data that was left by previous line "m=s.recvuntil('as a word.')".And the description of the functin recv() is "Receives up to numb bytes of data from the tube, and <u>**returns as soon as any quantity of data is available.**</u>".So it returns as soon as it read the data which was left by previous line.  
<br>
Q:How to solve the problem?  
A:You can easily solve the problem by using function recvall().





