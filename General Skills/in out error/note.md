# in out error 
## Question
>Can you utlize stdin, stdout, and stderr to get the flag from this [program](./in-out-error)? You can also find it in /problems/in-out-error_1_24ebc7186086f0f9a710de008628c561 on the shell server
### Hint
>Maybe you can split the stdout and stderr output?
## Solution
`Pseudo Code`  
```c
int main(void){
    char* output="\nWe're no strangers to love\nYou know the rules and so do I\nA full commitment's what I'm thinking of\nYou wouldn't get this from any other guy\n\nI just wanna tell you how I'm feeling\nGotta make you under";
    char* phrase="Please may I have the flag?";
        
    setvbuf(stdout,0,2,0);
    setvbuf(stderr,0,2,0);
    int len_flag=0
    int len_output=0
    char flag[];
    char input[28]
     
    if((fp_flag=fopen("flag.txt","r"))!=0){
    	if(fscanf(fp_flag,"%s",flag)!=-1){
    		close(fp_flag);
    	}
    	fscanf(fp_flag,"%s",&flag);
     }
     len_flag=strlen(flag);
     len_output=strlen(output);
    
     puts("Hey There!\nIf you want the flag you have to ask nicely for it.\nEnter the phrase \"Please may I have the flag?\" into stdin and you shall receive.");
     fgets(input,28,stdin);
     if(strcasecmp(input,phrase)!=0){
    	puts("Thank you for asking so nicely!\n");
    	for(int i=0;i<len_output;++i){
    		fputc(output[i],stdout);
    		fputc(flag[i%len_flag],stderr);
    	putchar('\n');
    	return 0;
     }else{
    	put("You didn't ask correctly :(\nNo flag for you");
    	return 0;
     }

```

` bash IO redirect`  
```bash
M>N
# "M" is a file descriptor, which defaults to 1, if not explicitly set.
# "N" is a filename.
# File descriptor "M" is redirect to file "N."
M>&N
# "M" is a file descriptor, which defaults to 1, if not set.
# "N" is another file descriptor.
```
`solution`   
```bash
./in-out-error 2>~/test
cat ~/test
```
~~~
picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}
~~~
### source:[IO redirect](http://tldp.org/LDP/abs/html/io-redirection.html)
