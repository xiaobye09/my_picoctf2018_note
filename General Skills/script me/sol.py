#!/usr/bin/env python
import pwn


pwn.context.log_level = 'error'
def get_problem(s):
	head=-1
	tail=-1
	for i in range(0,len(s)):
		if head==-1 and s[i]=='(':
			head=i
		if s[i]==')':
			tail=i

	return s[head:tail+1]
			
def legal(s):
	count={'(':0}
	for i in range(0,len(s)):
		if s[i]==')':
			if count['(']==0:
				return False
			else:
				count['(']=count['(']-1
		else:
			count['(']=count['(']+1
	return True
	
def rule(ls,rs):
	lcount=0
	rcount=0
	new=""
	if depth(ls)>depth(rs):
		if not legal(ls):
			new=ls+rs
		else:
			new=ls[0:len(ls)-1]+rs+')'
	elif depth(ls)<depth(rs):
		if not legal(rs):
			new=ls+rs
		else:
			new='('+ls+rs[1:len(rs)+1]
	else:
		new=ls+rs	
	return new

		
def depth(str):
	n=0
	max=0
	for i in range(0,len(str)):
		if str[i] == '(':
			n=n+1
			if n>max:	
				max=n
		else:
			n=n-1
	#print max
	return max
def solve(test):
	new = rule(test[0],test[1])
	if test>2:
		for x in test[2:len(test)+1]:
			new = rule(new,x)
	return new


p=pwn.remote("2018shell.picoctf.com", 7866)
	
p.recvuntil('warmup.')
#print p.recvuntil('> ')
test=get_problem(p.recvuntil('> ').split(" = ")[0].strip())
#print test
ans =  solve(test.replace(' ','').split('+'))
#print ans
p.sendline(ans)
result=p.recv()
#p.interactive()
count=0
while 'Correct' in result and count!=4:
	count+=1
	test=p.recvuntil('> ')
	test=get_problem(test.replace(' ',''))
	#print test
	ans=solve(test.split('+'))		
	#print ans
	p.sendline(ans)
	result=p.recv()
	#print result
else:
	print p.recv()#.split(':')[1].strip()
p.close()
