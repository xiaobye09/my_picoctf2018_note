# script me 
## Question
>Can you understand the language and answer the questions to retrieve the flag? Connect to the service with nc 2018shell.picoctf.com 7866


### Hint
>TMaybe try writing a python script?
## Solution
Observe the pattern and construct the rule function.

`Sometimes,the script will get the wrong length of string`  
[script](./sol.py)

### flag
~~~
picoCTF{5cr1pt1nG_l1k3_4_pRo_45ca3f85}
~~~
