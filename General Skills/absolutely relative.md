# absolutely relative 
## Question
>In a filesystem, everything is relative ¯\_(ツ)_/¯. Can you find a way to get a flag from this program? You can find it in /problems/absolutely-relative_0_d4f0f1c47f503378c4bb81981a80a9b6 on the shell server. Source.
## Hint
>Do you have to run the program in the same directory? (⊙.☉)7  
>Ever used a text editor? Check out the program 'nano'## Solution  
## Solution
```c
#include <stdio.h>
#include <string.h>

#define yes_len 3
const char *yes = "yes";

int main()
{
    char flag[99];
    char permission[10];
    int i;
    FILE * file;


    file = fopen("/problems/absolutely-relative_0_d4f0f1c47f503378c4bb81981a80a9b6/flag.txt" , "r");//absolute path
    if (file) {
    	while (fscanf(file, "%s", flag)!=EOF)
    	fclose(file);
    }   
	
    file = fopen( "./permission.txt" , "r");                    //relative path,this is the key point ,./ means current directory in terminal
    if (file) {
    	for (i = 0; i < 5; i++){
            fscanf(file, "%s", permission);
        }
        permission[5] = '\0';
        fclose(file);
    }
    
    if (!strncmp(permission, yes, yes_len)) {
        printf("You have the write permissions.\n%s\n", flag);
    } else {
        printf("You do not have sufficient permissions to view the flag.\n");
    }
    
    return 0;
}
```
After reading the source code,I know that I have to add file called permission.txt and write a string "yes" in it.  
I don't have permission to add file under /problems/absolutely-relative_0_d4f0f1c47f503378c4bb81981a80a9b6,so I back to my home and create permission.txt and execute the program under home directory.
![1](./picture/5.png)

## Flag
~~~
picoCTF{3v3r1ng_1$_r3l3t1v3_befc0ce1}
~~~

