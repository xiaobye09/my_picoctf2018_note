# you can't see me 
## Question
>'...reading transmission... Y.O.U. .C.A.N.'.T. .S.E.E. .M.E. ...transmission ended...' Maybe something lies in /problems/you-can-t-see-me_3_1a39ec6c80b3f3a18610074f68acfe69.
### Hint
>TWhat command can see/read files?   
>What's in the manual page of ls?   
## Solution
### Solution1
After I cd to /problems/you-can-t-see-me_3_1a39ec6c80b3f3a18610074f68acfe69 and use ls to show the things that under the directory,we found nothing,so I guess that it is hidden.Then,I try command "ls -al" to find to see more detailed infomation and I found that there is a suspicious file having the name '.' which is same as current directory.
![1](General%20Skills/picture/4.png)
I try to open it by vim ,but vim always open the directory one......
So I came up with using inode number to open the file.(Using "ls -ali" ,-i:show inode number)
Command 'find' have a parameter that can execute script so I choose to use 'find' to finish the mission
```bash
# starting-point represent to the directory that 'find' start.If no starting-point is specified, `.' is assumed.
# n represent inode number of the file
# every arguments after -exec should be seperated by space
# Should add backslash before semicolon
find starting-point -inum n -exec cat {} \;  

# find the file by name 
find starting-point -name n

# find the file by name  and the match is case insensitive
find starting-point -iname n
```
```bash
????@pico-2018-shell:/problems/you-can-t-see-me_3_1a39ec6c80b3f3a18610074f68acfe69$ ls -ali
total 60
15622213 drwxr-xr-x   2 root       root        4096 Mar 25 19:57 .
15622218 -rw-rw-r--   1 hacksports hacksports    57 Mar 25 19:57 .  
13824001 drwxr-x--x 556 root       root       53248 Mar 25 19:58 ..

????@pico-2018-shell:/problems/you-can-t-see-me_3_1a39ec6c80b3f3a18610074f68acfe69$ find . -inum 15622218 -exec cat {} \; 
picoCTF{j0hn_c3na_paparapaaaaaaa_paparapaaaaaa_cf5156ef}
```
### Solution2 
Here is a simple way to do
```bash 
#The asterisk as a metacharacter (*)
#The asterisk is a more universally known metacharacter, and is used to mean zero or more of any character when searching for a pattern.
cat .*
```
### Solution3
```bash
#try TAB to trigger automatic completion
cat ./
#Then it will show the hidden characters behind it.
cat ./.\ \
```
## Flag
~~~
picoCTF{j0hn_c3na_paparapaaaaaaa_paparapaaaaaa_cf5156ef}
~~~
- ## Useful tool
    - ### find
        - #### -exec
        >Execute  command;  true  if  0  status is returned.  All following arguments to find are
        taken to be arguments to the command until an argument consisting of ';' is encountered.
        The  string  '{}' is replaced by the current file name being processed everywhere it occurs in the arguments to the command, not just in arguments where it  is  alone,  as  in
        some  versions  of  find.   **Both of these constructions might need to be escaped (with a
        `\')** or quoted to protect them from expansion by the shell.  See  the  EXAMPLES  section
        for examples of the use of the -exec option.  The specified command is run once for each
        matched file.  The command is executed in the starting directory.  There are unavoidable
        security  problems  surrounding use of the -exec action; you should use the -execdir option instead.


