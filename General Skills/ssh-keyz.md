# ssh-keyz 
## Question
>As nice as it is to use our webshell, sometimes its helpful to connect directly to our machine. To do so, please add your own public key to ~/.ssh/authorized_keys, using the webshell. The flag is in the ssh banner which will be displayed when you login remotely with ssh to with your username.
### Hint
>Tkey generation [tutorial](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)  
>We also have an expert demonstrator to help you along. [link](https://www.youtube.com/watch?v=3CN65ccfllU&list=PLJ_vkrXdcgH-lYlRV8O-kef2zWvoy79yP&index=4)
## Solution
STEP1: check if ~/.ssh exist.   
If not, add a new .ssh directory under ~/.  
If already exist and already have the rsa key in it, use that public key and skip to step3.

![test](General%20Skills/picture/1.png)

STEP2:create a RSA key.
```bash
ssh-keygen -t rsa -C "email"
```
![test](General%20Skills/picture/2.png)
STEP3:Copy public key into clip board
```bash
xclip -sel clip < id_rsa.pub
```

STEP4:Open the web shell and cd to ~/.ssh(if .ssh not exist,create one),and copy the public key into authorized_key.
```bash
cat > authorized_key
```
![test](General%20Skills/picture/3.png)
STEP6:change your file mode to 600(only owner can read and write it)
```bash
chmod 600 authorized_key
```
STEP6:Back to your shell and ssh to 2018shell4.picoctf.com,then you will get the flag.
```bash 
ssh username@2018shell4.picoctf.com
```
~~~
picoCTF{who_n33ds_p4ssw0rds_38dj21}
~~~

