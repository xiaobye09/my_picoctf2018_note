# learn gdb 
## Question
Using a debugging tool will be extremely useful on your missions. Can you run this [program](./run) in gdb and find the flag? You can find the file in /problems/learn-gdb_0_716957192e537ac769f0975c74b34194 on the shell server.

### Hint
>Try setting breakpoints in gdb  

>Try and find a point in the program after the flag has been read into memory to break on  

>Where is the flag being written in memory?  
## Solution
`Set break point(b main) at main and run(r)`  
![1](./picture/line_20190909_004741.png)  
`show the disassembly code(disas) and then we can see <decrypt_flag>  at 0x0000000000400905.We want to observe this function so we set break point at 0x0000000000400905(b *0x0000000000400905 and continue(c)).`   
![1](./picture/line_20190909_004949.png)  
`We aren't in function <decrypt_flag> yet.We have to step into it(si).Before we step into it,we should set a break point after this call instruction(b *0x40090a) so that we can back to main function by continue(c) easily.`    
![1](./picture/line_20190909_005311.png)   
`After step into <decrypt_flag> and show disassembly code(disas or disas decrypt_flag),we can find key function - malloc,and I suspect that it malloc the space of flag so I set break point at 0x00000000004007ae(b *0x00000000004007ae) , continue(c) and observe the value in rax(info reg rax, i r rax or i r).`  
![1](./picture/line_20190909_010239.png)  
![1](./picture/line_20190909_010740.png)  
`After we record address that we find in rax, continue(c) and observe the space that it point to(x\s 0x602260).YA,we get the flag!!.`  
![1](./picture/line_20190909_011041.png)   
## flag
```
picoCTF{gDb_iS_sUp3r_u53fuL_a6c61d82}
```

