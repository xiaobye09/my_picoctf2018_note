# CTF note for myself

## Title
- [General Skills](#general-skills)
- [Binary exploitation](#binary-exploitation)
- [Cryptography](#cryptography)
- [Forensics](#forensics)
- [Web Exploitation](#web-exploitation)

## General Skills
<a href="General%20Skills/note.md">Tool Set</a><br>
<a href="General%20Skills/ssh-keyz.md">ssh-keyz(ssh-keygen)</a><br>
<a href="General%20Skills/what%20base%20is%20this.md">what base is this?(pwntools/binascii/re)</a><br>
<a href="General%20Skills/you%20can't%20see%20me.md">you can't see me?(find/cat/ls)</a><br>
<a href="General%20Skills/absolutely%20relative.md">absolutely relative</a><br>
<a href="General%20Skills/in%20out%20error/note.md">in out error(bash IO redirect)</a><br>
<a href="General%20Skills/learn%20gdb/note.md">learn gdb(gdb)</a><br>
<a href="General%20Skills/script%20me/note.md">script me(python/pwn)</a><br>
<a href="General%20Skills/store/note.md">store</a><br>
<a href="General%20Skills/roulette/note.md">roulette</a><br>


## Binary Exploitation
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/leak-me.md">leak-me</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/buffer%20overflow%201.md">buffer overflow 1</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/shellcode.md">shellcode(pwntools)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/buffer%20overflow%202.md">buffer overflow 2(dmesg/tail)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/got-2-learn-libc.md">got-2-learn-libc(dmesg/tail/gdb/pwntools/re)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/echooo/note.md">echooo(fuzz/user-defined format string)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Binary%20Exploitation/authenticate/note.md">authenticate(fuzz/user-defined format string)</a></td>  
        </tr>
    </tbody>
</table>

## Cryptography
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td markdown="span"><a href="Cryptography/hertz%202.md">hertz 2</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Cryptography/caesar%20cipher%202.md">caesar cipher 2</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Cryptography/Safe%20RSA/note.md">Safe RSA(gmpy2)</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Cryptography/rsa-madlibs/note.md">rsa-madlibs</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Cryptography/HEEEEEEERE'S Johnny/note.md">HEEEEEEERE'S Johnny!(unshadow/john)</a></td>
        </tr>
    </tbody>
</table>

## Forensics  
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td markdown="span"><a href="Forensics/now%20you%20don't.md">now you don't(stegsolve)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="/Forensics/Ext%20Super%20Magic.md">Ext Super Magic(binwalk/file/hexedit/find/e2fsck/fsck/mount/umount)</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Forensics/Recovering%20From%20the%20Snap.md">Recovering From the Snap(binwalk)</a></td>  
        </tr>
    </tbody>
</table>

## Reversing
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td markdown="span"><a href="Reversing/be-quick-or-be-dead-1.md">be-quick-or-be-dead-1</a></td>  
        </tr>
        <tr>
            <td markdown="span"><a href="Reversing/quackme.md">quackme</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Reversing/assembly-2.md">assembly-2</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Reversing/be-quick-or-be-dead-2/note.md">be-quick-or-be-dead-2(objdump)</a></td>  
        </tr>
    </tbody>
</table>

## Web Exploitation
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td markdown="span"><a href="Web%20Exploitation/Mr.Robots.md">Mr.Robots</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Web%20Exploitation/Buttons.md">Buttons(curl/rev/cut)</a></td>
        </tr>
        <tr>
            <td markdown="span"><a href="Web%20Exploitation/The%20Vault.md">The Vault</a></td>
        </tr>
    </tbody>
</table>

## Others
<table>
    <thead>
        <tr class="header">
            <th>Problem</th>
        </tr>
    </thead>
    <tbody>
        <td markdown="span"><a href="/others/解決在64bits系統下無法開啟32bits可執行文件問題.pdf">解決在64bits系統下無法開啟32bits可執行文件</a></td>
    </tbody>
</table>
