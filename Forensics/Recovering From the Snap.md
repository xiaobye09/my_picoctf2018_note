# Recovering From the Snap
## Question
>There used to be a bunch of [animals](./resource/animals.dd) here, what did Dr. Xernon do to them?
### Hint
>Some files have been deleted from the disk image, but are they really gone?

## Solution
First I tried to open this file but it failed.
Then I went to [see](https://www.whatisfile.com/dd) what is the **.dd** file.

###### Overview
>Those who are looking for disk image file extension can install DD. It is a replica of a hard disk drive which extension is .dd. Like other file formats, you can recognize the DD file by looking at its extension .dd. The utility creates disk images by commanding line interface in the system of UNIX and LINUX OS.

## 1.Install **binwalk**
>Binwalk is a tool for searching a given binary image for embedded files and executable code. Specifically, it is designed for identifying files and code embedded inside of firmware images.
```
brew install binwalk
```
## 2.Use binwalk to go through the file
```
binwalk animals.dd
```
And I got the result below.   
```
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
39424         0x9A00          JPEG image data, JFIF standard 1.01
39454         0x9A1E          TIFF image data, big-endian, offset of first image directory: 8
672256        0xA4200         JPEG image data, JFIF standard 1.01
1165824       0x11CA00        JPEG image data, JFIF standard 1.01
1556992       0x17C200        JPEG image data, JFIF standard 1.01
1812992       0x1BAA00        JPEG image data, JFIF standard 1.01
1813022       0x1BAA1E        TIFF image data, big-endian, offset of first image directory: 8
2136576       0x209A00        JPEG image data, JFIF standard 1.01
2136606       0x209A1E        TIFF image data, big-endian, offset of first image directory: 8
2607616       0x27CA00        JPEG image data, JFIF standard 1.01
2607646       0x27CA1E        TIFF image data, big-endian, offset of first image directory: 8
3000832       0x2DCA00        JPEG image data, JFIF standard 1.01
3000862       0x2DCA1E        TIFF image data, big-endian, offset of first image directory: 8
```
Then I tried to extract the file.  
Extraction Options:  
	-D, --dd=< type:ext:cmd >      Extract <type> signatures, give the files an extension of <ext>, and execute <cmd>
```
binwalk -D 'jpeg image' animals.dd
```
Finally,generated a [folder](./resource/_animals.dd.extracted) with pictures containing flag.

## flag 
```
picoCTF{th3_5n4p_happ3n3d}
```