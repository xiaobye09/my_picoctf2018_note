# Ext Super Magic

## Question
>We salvaged a ruined Ext SuperMagic II-class mech recently and pulled the filesystem out of the black box. It looks a bit corrupted, but maybe there's something interesting in there. You can also find it in /problems/ext-super-magic_3_debae27da9f20eec855067f3e83b45f3 on the shell server.


### Hint
>Are there any [tools](https://en.wikipedia.org/wiki/Fsck) for diagnosing corrupted filesystems? What do they say if you run them on this one?  
>How does a linux machine know what [type](https://www.garykessler.net/library/file_sigs.html) of file a [file](https://linux.die.net/man/1/file) is?  
>You might find this [doc](http://www.nongnu.org/ext2-doc/ext2.html) helpful    
>Be careful with [endianness ](https://en.wikipedia.org/wiki/Endianness)when making edits.  
>Once you've fixed the corruption, you can use /sbin/[debugfs](https://linux.die.net/man/8/debugfs) to pull the flag file out.

## Solution
`I have no idea at all,so I look up the tips,and I found some key point in [tools](https://en.wikipedia.org/wiki/Fsck).
`Now,I understand that this file is a ruined ** ext2(The Second Extended File System) **`
`I try fsck(file system consistency check) and got a message and we can see that fsck in my system is for ext4,so I have to find the ext2 version`
![1](./picture/6.png)

`Then I try e2fsck,and got some message that tell me something wrong with superblocks and magic number.I have no idea about these two things!!!Therefore,I made some effort to find key word in this page` [doc](http://www.nongnu.org/ext2-doc/ext2.html)
![1](./picture/7.png)


`First,,ext2 have a field that store the information about the configuration of filesystem called **superblocks(it is a structure)**,and it is located at **byte offset 1024** from the start of volume.`
![1](./picture/1.png)
![1](./picture/2.png)
`Second,there are two bytes value that is used to record the file signature called **s_magic(equal to 0xEF53) **in superblocks`
![1](./picture/3.png)
`Now I know where the problem is,and I open "hexedit" go to offset 0x438(1080) and modified two bytes content to **53 EF(little endian)**`
![1](./picture/4.png)
`It is now corrected.`
![1](./picture/5.png)
`Use **"binwalk"** and extract all files and then we can find flag.txt in the file`
```bash
binwalk -e ext-super-magic1.img
find ./ -iname "flag*"
```
`By the way,we also can use **mount** to mount the ext2 on a directory.`
```bash 
mkdir mountpoint
sudo mount ext-super-magic.img ./mountpoint

###When we found the flag ,we have to umount it
sudo umount ./mountpoint
```
`Or use debugfs(debug filesystem) to extraxt the flag`
```bash 
mkdir tmp
debugfs ext-super-magic.img 
#under debug mode
debugfs:  rdump ./ ./tmp   #recursively dump the file from ./ to ./tmp(native dir)
debugfs:  q                #quit   

#If you find the target file by "ls" in debug mode,you also can use dump to dump a inode to native filesystem
debugfs:  ls                     #find the flag
debugfs:  dump flag.jpg flag.jpg #dump the flag.jpg to the current directory on our native filesystem and named as flag.jpg
```
## Flag
![1](./picture/flag.jpg)


- ## Useful tool
    - ###  find
        - #### -iname pattern
        >Like -name, but the match is case insensitive.  For example, the patterns 'fo*' and  'F??'  match  the  file names 'Foo', 'FOO', 'foo', 'fOo', etc.  The pattern '*foo*` will also match a file called '.foobar'.
    - ### binwalk
        - ####  -e, --extract
        >Automatically extract known file types
    - ### mount(mount a filesystem)
        - #### mount [-fnrsvw] [-t fstype] [-o options] device dir
        >**Most devices are indicated by a filename (of a block special device)**, like /dev/sda1, but there are other possibil
       ities.  For example, in the case of an NFS mount, device may look like knuth.cwi.nl:/dir.  It is also  possible  to
       indicate a block special device using its filesystem label or UUID (see the -L and -U options below), or its parti
       tion label or UUID.  Partition identifiers are supported for example for GUID Partition Tables (GPT).
    - ### e2fsck,fsck
    - ### debugfs
        - #### ?
        >show the usage
        - #### rdump <directory\>... <native directory\>
        > Recursively dump a directory to the native filesystem
        - #### dump_inode [-p] <file\> <output_file\>
        >Dump an inode out to a file(copy to native filesystem which is our local filesystem)
    
