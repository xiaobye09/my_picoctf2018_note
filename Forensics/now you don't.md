# now you don't
## Question
>We heard that there is something hidden in this picture. Can you find it?
### Hint
>There is an old saying: if you want to hide the treasure, put it in plain sight. Then no one will see it.
>Is it really all one shade of red?

## Solution
Stegsolve can easily solve this proble
>switch to red plain 1 or red plain 2 then you will get the flag
~~~
picoCTF{n0w_y0u533_m3}
~~~


## Install stegsolve
1.install java first 
~~~
sudo apt install default-jre
~~~
2.download stegsolve
~~~
wget http://www.caesum.com/handbook/Stegsolve.jar -O stegsolve.jar
chmod +x stegsolve.jar
~~~
3.double click to open the .jar file,then the program will be executed.