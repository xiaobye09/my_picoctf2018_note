#include<stdio.h>
#define NUMBER 1026
int fib_iter(int n){
	int a=0;
	int b=1;
	int m;
	if(n==0) return a;
	else if(n==1) return b;
	
	for(int i=2;i<=n;++i){
		m=a+b;
		a=b;
		b=m;
	}
	return b;
}
int main(void){
	printf("%x",fib_iter(NUMBER));
	return 0;
}
	
