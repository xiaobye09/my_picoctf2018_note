from pwn import *

def decrypt(c,k):
	m=""
	for i in range(0,len(c)):
		m+=chr(ord(c[i])^ord(k[i%4]))
		if (i%4)==3:
			#Be careful of little endian add 1
			k=("%x" %(int(k[::-1].encode('hex'),16)+1)).decode('hex')[::-1]
	return m


cypher= "28f2 6998 1acf 4c8c 2ef3 6fa8 3df2 6898 32fa 6994 34c4 7992 2fee 6f99 3cfe 5594 01f5 5595 04    c4 6e98 0cfe 5591 02e8 7ea8 53fe 3bcf 5da3 39c3 1b".replace(' ','').decode('hex')

p=process('./fib')
key=p.recvall().strip().decode('hex')[::-1]#little endian
p.close()

#print key

print decrypt(cypher,key)
