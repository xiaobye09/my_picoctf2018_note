# be-quick-or-be-dead-2
## Question
>As you enjoy this [music](https://www.youtube.com/watch?v=CTt1vk9nM9c) even more, another executable [be-quick-or-be-dead-2](./be-quick-or-be-dead-2) shows up. Can you run this fast enough too? You can also find the executable in /problems/be-quick-or-be-dead-2_2_7e92e9cc48bad623da1c215c192bc919.
### Hint
>Can you call stuff without executing the entire program?  
>What will the key finally be?  

## Solution
`psedo code`
```c
int key;
int flag;
void get_key(){
    printf("Calculating Key");
    key=calculate_key();
    printf("Done calculating key");
}
void print_flag(){
    printf("Printing flag:");
    decrypt_flag(&key)
    printf(flag);
}
int calculate_key(){
    return fib(1026);
}
int fib(int n){
    if(n<=1) return n;
    return fib(n-1)+fib(n-2);
}
void decryt_flag(char *pkey){
    char *pflag=&flag;
    for(int i=0;i<=56;++i){
        pflag[i]=pkey[i%4]^pflag[i]
        if((i%4)!=3) continue;
        else key=key+1;
    }
}
```


>Since the fibonacci function of be-quick-or-be-dead-2 is recursive which time complexity is O(2^n),and it want the number of fib(1026),it isn't possible to finish in reasonable time.


solution1:
>We build an iterative version of fibonacci and then calculate fib(1026) get the key.(Be careful of little endian.The key that you get is reverse one)

solution2:
>We guess that the first four char in cyphertext is 'pico' and then we can reverse the key.


Soloution1 and 2 are in sol1 and sol2,respectively.  
`IMPORTANT!!!Be careful of little endian of the flag.We have to reverse it first , add 1,and then reverse it again.`


```
picoCTF{the_fibonacci_sequence_can_be_done_fast_7e188834}
```
