#!/usr/bin/env python

#We know that the cypher text is 57byte and it start from 0x1080 in file

def decrypt(c,k):
	p=""
	for i in range(0,57):#0~56
		p+=chr(ord(c[i])^ord(k[i%4]))
		if (i%4)==3:
			#be careful of little endian
			#We have to reverse the key and add 1 ,then reverse it again.
			k=(("%x" %(int(k[::-1].encode('hex'),16)+1)).decode('hex'))[::-1]
	return p




cypher= "28f2 6998 1acf 4c8c 2ef3 6fa8 3df2 6898 32fa 6994 34c4 7992 2fee 6f99 3cfe 5594 01f5 5595 04c4 6e98 0cfe 5591 02e8 7ea8 53fe 3bcf 5da3 39c3 1b".replace(' ','').decode('hex')
#We get the key from fib which we write.
key="%02x" %(ord('p')^ord(cypher[0]))
key+="%02x" %(ord('i')^ord(cypher[1]))
key+="%02x" %(ord('c')^ord(cypher[2]))
key+="%02x" %(ord('o')^ord(cypher[3]))
key=key.decode('hex')
#print key[::-1]
#print len(cypher)
print decrypt(cypher,key)

