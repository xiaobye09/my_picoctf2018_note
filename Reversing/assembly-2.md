# assembly-2 
## Question
>What does asm2(0xe,0x21) return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](./assembly-2/loop_asm_rev.S) located in the directory at /problems/assembly-2_3_c3ee3603bd2a8e682f1d64cf6dfd21fb.
### Hint
>assembly [conditions](https://www.tutorialspoint.com/assembly_programming/assembly_conditions.htm)
## Solution
```asm
.intel_syntax noprefix
.bits 32
	
.global asm2

asm2:
	push   	ebp
	mov    	ebp,esp
	sub    	esp,0x10


;local var initialize
	mov    	eax,DWORD PTR [ebp+0xc]
	mov 	DWORD PTR [ebp-0x4],eax
	mov    	eax,DWORD PTR [ebp+0x8]
	mov	DWORD PTR [ebp-0x8],eax
	jmp    	part_b


;loop
part_a:	
	add    	DWORD PTR [ebp-0x4],0x1
	add	DWORD PTR [ebp+0x8],0x41
part_b:	
	cmp    	DWORD PTR [ebp+0x8],0x9886
	jle    	part_a


;return eax
	mov    	eax,DWORD PTR [ebp-0x4]
	mov	esp,ebp
	pop	ebp
	ret

```
PseudoCode  
```c
int32_t asm2(int32_t a,int32_t b){
    int32_t m,n;
    for(m=b,n=a;b<=0x9886;m+=0x1,b+=0x41);
    return m;
}
```    
## Flag
~~~
0x27a
~~~

