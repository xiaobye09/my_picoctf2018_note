# Mr. Robots
## Question
>You find this when searching for some music, which leads you to be-quick-or-be-dead-1. Can you run it fast enough? You can also find the executable in /problems/be-quick-or-be-dead-1_2_83a2a5193f0340b364675a2f0cc4d71e. 
### Hint
>What will the key finally be?

## Solution
## I come up with three idea.  

1. ### Removing function set_timer in main.  
2. ### Making alert signal being trigger lately so that function calculate_key will have enough time.    
3. ### Replacing instruction(callq  400580 <exit @ plt>) in function alarm_handler by nop nop nop leaveq retq.  

```
picoCTF{why_bother_doing_unnecessary_computation_d0c6aace}
```
