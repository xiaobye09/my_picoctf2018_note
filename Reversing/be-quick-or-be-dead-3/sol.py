import pwn

def decrypt(c,k):
	flag=""
	for i in range(0,len(c)):
		key=pwn.p32(k)
		#print "c: %d,key: %d" %(ord(c[i]),ord(key[i%4]))
		flag+=chr(ord(c[i])^ord(key[i%4]))
		if (i%4)==3:
			k=k+1

	return flag


cypher="9ae77e4d a8da 5b59 88f7 7343 80e7 7e7d9efc 2d45 9def 704f 99e0 7a7d 97fa 6a7d93be 7f12 91b9 7b1a89".replace(' ','').decode('hex')

print cypher


p=pwn.process('./sim')

key=int(p.recvline().strip(),16)
p.close()

print decrypt(cypher,key)
