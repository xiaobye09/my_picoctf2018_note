#include<stdio.h>
#define N 100021
//#define N 7
int arr[100025];
int recur_calc(int n){
	int i=n;
	int result;
	if(i<=4){
		result=i*i+9029;
	}else{
        result=(recur_calc(i-1)-recur_calc(i-2))+(recur_calc(i-3)-recur_calc(i-4))+(recur_calc(i-5)*4660);
    }   
    return result;
}
int iter_calc(int n){
	for(int i=0;i<=n;i++){
		if(i<=4){
			arr[i]=i*i+9029;
		}else{
			arr[i]=(arr[i-1]-arr[i-2])+(arr[i-3]-arr[i-4])+(arr[i-5]*4660);
		}
	}
	return arr[n];
}
int main(void){
//	for(int i =0;i<=N;i++){		
//		printf("recur: %d\n",recur_calc(i));
	printf("%x\n",iter_calc(N));
//		printf("================\n");
//	}
	return 0;
}
