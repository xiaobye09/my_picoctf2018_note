# quackme
## Question
>Can you deal with the Duck Web? Get us the flag from this program. You can also find the program in /problems/quackme_2_45804bbb593f90c3b4cefabe60c1c4e2.
### Hint
>Objdump or something similar is probably a good place to start.
## Solution
The key point
```asm 
 ;global var 
 0x8048858 = sekrutBuffer
 DWORD PTR ds:0x804a038=str = "You have now entered the Duck Web, and you're in for a honkin' good time"
 ;local var
 [ebp-0x14]=input
 [ebp-0x10]=input_len
 [ebp-0xc]=pBuf
 [ebp-0x18]=count_a
 [ebp-0x1c]=count_b
 [ebp-0x1d]=check_ch
 08048642 <do_magic>:
 8048642:	55                   	push   ebp    
 8048643:	89 e5                	mov    ebp,esp    
 8048645:	83 ec 28             	sub    esp,0x28                ;construct stack frame
 
 
 8048648:	e8 8e ff ff ff       	call   80485db <read_input>    
 804864d:	89 45 ec             	mov    DWORD PTR [ebp-0x14],eax
 8048650:	83 ec 0c             	sub    esp,0xc                 ;input = read_input()
 
 
 8048653:	ff 75 ec             	push   DWORD PTR [ebp-0x14]        
 8048656:	e8 35 fe ff ff       	call   8048490 <strlen@plt>  
 804865b:	83 c4 10             	add    esp,0x10                
 804865e:	89 45 f0             	mov    DWORD PTR [ebp-0x10],eax;input_len = strlen(input)  
 
 8048661:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]  
 8048664:	83 c0 01             	add    eax,0x1  
 8048667:	83 ec 0c             	sub    esp,0xc  
 804866a:	50                   	push   eax   
 804866b:	e8 f0 fd ff ff       	call   8048460 <malloc@plt>   
 8048670:	83 c4 10             	add    esp,0x10  
 8048673:	89 45 f4             	mov    DWORD PTR [ebp-0xc],eax ;pBuf=malloc(input_len+1)
 
 
 8048676:	83 7d f4 00          	cmp    DWORD PTR [ebp-0xc],0x0  
 804867a:	75 1a                	jne    8048696 <do_magic+0x54>   
 
 ;if(pBuf==0){
     804867c:	83 ec 0c             	sub    esp,0xc   
     804867f:	68 84 88 04 08       	push   0x8048884  
     8048684:	e8 e7 fd ff ff       	call   8048470 <puts@plt>   
     8048689:	83 c4 10             	add    esp,0x10   
     804868c:	83 ec 0c             	sub    esp,0xc  
     804868f:	6a ff                	push   0xffffffff   
     8048691:	e8 ea fd ff ff       	call   8048480 <exit@plt>
 }
 else{
     8048696:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]   
     8048699:	83 c0 01             	add    eax,0x1   
     804869c:	83 ec 04             	sub    esp,0x4  
     804869f:	50                   	push   eax   
     80486a0:	6a 00                	push   0x0  
     80486a2:	ff 75 f4             	push   DWORD PTR [ebp-0xc]   
     80486a5:	e8 16 fe ff ff       	call   80484c0 <memset@plt>    ;memset(pBuf,0,input_len+1)
     80486aa:	83 c4 10             	add    esp,0x10   
     
     
     80486ad:	c7 45 e4 00 00 00 00 	mov    DWORD PTR [ebp-0x1c],0x0;count_b=0
     80486b4:	c7 45 e8 00 00 00 00 	mov    DWORD PTR [ebp-0x18],0x0;count_a=0
     80486bb:	eb 4e                	jmp    804870b <do_magic+0xc9>
     while(input_len>count_b){
         80486bd:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]   
         80486c0:	05 58 88 04 08       	add    eax,0x8048858             ;eax = sekrutBuffer+count_a
         80486c5:	0f b6 08             	movzx  ecx,BYTE PTR [eax]        ;ecx = *eax
         
         80486c8:	8b 55 e8             	mov    edx,DWORD PTR [ebp-0x18]   
         80486cb:	8b 45 ec             	mov    eax,DWORD PTR [ebp-0x14]   
         80486ce:	01 d0                	add    eax,edx                   ;eax = input+count_a
         80486d0:	0f b6 00             	movzx  eax,BYTE PTR [eax]        ;eax = *eax
         
         80486d3:	31 c8                	xor    eax,ecx                   ;eax = eax^ecx    (key point!!)
         
         80486d5:	88 45 e3             	mov    BYTE PTR [ebp-0x1d],al    ;check_ch = al
         
         80486d8:	8b 15 38 a0 04 08    	mov    edx,DWORD PTR ds:0x804a038  ;edx=str
         80486de:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]    
         80486e1:	01 d0                	add    eax,edx                     ;eax = str+count_a      
         80486e3:	0f b6 00             	movzx  eax,BYTE PTR [eax]          ;eax = *eax 
         
         80486e6:	3a 45 e3             	cmp    al,BYTE PTR [ebp-0x1d]   
         80486e9:	75 04                	jne    80486ef <do_magic+0xad>    
         if(al==check_ch){
             80486eb:	83 45 e4 01          	add    DWORD PTR [ebp-0x1c],0x1   ;count_b++
             80486ef:	83 7d e4 19          	cmp    DWORD PTR [ebp-0x1c],0x19   
             80486f3:	75 12                	jne    8048707 <do_magic+0xc5>   
             if(count_b==0x19){
                 80486f5:	83 ec 0c             	sub    esp,0xc   
                 80486f8:	68 ab 88 04 08       	push   0x80488ab   
                 80486fd:	e8 6e fd ff ff       	call   8048470 <puts@plt>   
                 8048702:	83 c4 10             	add    esp,0x10  
                 8048705:	eb 0c                	jmp    8048713 <do_magic+0xd1> ;break
             }
         }
         8048707:	83 45 e8 01          	add    DWORD PTR [ebp-0x18],0x1   ;count_a++
         804870b:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]   ;while loop
         804870e:	3b 45 f0             	cmp    eax,DWORD PTR [ebp-0x10]   
         8048711:	7c aa                	jl     80486bd <do_magic+0x7b>   
         
    }
 }
 8048713:	c9                   	leave  
 8048714:	c3                   	ret    
```
xor operation
```asm
80486bd:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]   
         80486c0:	05 58 88 04 08       	add    eax,0x8048858             ;eax = sekrutBuffer+count_a
         80486c5:	0f b6 08             	movzx  ecx,BYTE PTR [eax]        ;ecx = *eax
         
         80486c8:	8b 55 e8             	mov    edx,DWORD PTR [ebp-0x18]   
         80486cb:	8b 45 ec             	mov    eax,DWORD PTR [ebp-0x14]   
         80486ce:	01 d0                	add    eax,edx                   ;eax = input+count_a
         80486d0:	0f b6 00             	movzx  eax,BYTE PTR [eax]        ;eax = *eax
         
         80486d3:	31 c8                	xor    eax,ecx                   ;eax = eax^ecx    (key point!!)
```
check if input is desired form
```asm
80486d5:	88 45 e3             	mov    BYTE PTR [ebp-0x1d],al    ;check_ch = al
         
         80486d8:	8b 15 38 a0 04 08    	mov    edx,DWORD PTR ds:0x804a038  ;edx=str
         80486de:	8b 45 e8             	mov    eax,DWORD PTR [ebp-0x18]    
         80486e1:	01 d0                	add    eax,edx                     ;eax = str+count_a      
         80486e3:	0f b6 00             	movzx  eax,BYTE PTR [eax]          ;eax = *eax 
         
         80486e6:	3a 45 e3             	cmp    al,BYTE PTR [ebp-0x1d]   
         80486e9:	75 04                	jne    80486ef <do_magic+0xad>
```
solution script
```python
secret = ('2906164f2b35301e511b5b144b085d2b56475750164d51515d').decode('hex')
data = "You have now entered the Duck Web, and you're in for a honkin' good time."
flag=''
for i in range(len(secret)):
	flag+=chr(ord(secret[i])^ord(data[i]))

print flag

```
```
picoCTF{qu4ckm3_35246994}

```
